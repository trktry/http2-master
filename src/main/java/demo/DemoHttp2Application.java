package demo;

import org.eclipse.jetty.servlets.PushCacheFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
@EnableAutoConfiguration
public class DemoHttp2Application {

    public static void main(String[] args) {
        //SpringApplication.run(DemoHttp2Application.class, args);


        String json = "{'winCondition':'HIGH_SCORE',"
                + "'name':'Bowling',"
                + "'round':4,"
                + "'lastSaved':1367702411696,"
                + "'dateStarted':1367702378785,"
                + "'players':["
                + "{'name':'  player1  ','history':[10,8,6,7,8],'color':-13388315,'total':39},"
                + "{'name':'  player2  ','history':[6,10,5,10,10],'color':-48060,'total':41}"
                + "]}";

        X calisma = new X();

        try {
            calisma.postHttp2("http://www.roundsapp.com/post", json);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Bean
    public EmbeddedServletContainerCustomizer jettyHttp2Customizer(ServerProperties serverProperties) {
        return new JettyHttp2Customizer(serverProperties);
    }

    @Bean
    public FilterRegistrationBean pushCacheFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new PushCacheFilter());
        return registration;
    }
}
