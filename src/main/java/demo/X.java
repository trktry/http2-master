package demo;

import okhttp3.*;

import java.io.IOException;

public class X {

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    void postHttp2(String url, String json) throws IOException {



        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        OkHttpClient client = new OkHttpClient();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.out.println("fail oldu");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                System.out.println("response geldi.");
                System.out.println(response.body().string());
            }

        });

        System.out.println("request sent.");
    }
}
